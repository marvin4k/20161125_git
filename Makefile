ZETTEL_NAME = main

TEX_ALL = main.tex talk.tex end.tex backup.tex

###
###
###

# for cleaning
TEX_ENDINGS = pdf aux toc log out ind idx ilg bbl blg lot lof lol nls nav snm nlo vrb pyg aex

OPTION = -file-line-error -halt-on-error -shell-escape -jobname=$(ZETTEL_NAME)

PDFLATEX=/usr/bin/pdflatex
BIBTEX=/usr/bin/bibtex
# maybe see https://wiki.math.cmu.edu/iki/wiki/tips/20140310-zathura-fsearch.html
VIEWER=xdg-open 2>/dev/null

SUBDIRS=gnuplot svg

all: subdirs $(ZETTEL_NAME).pdf

#------------------------------------
#
# Plumbing Layer...
#
#------------------------------------

# generating default- and clean-targets for all subdirs

subdirs: $(SUBDIRS)

subdirs_clean: $(addsuffix _clean,$(SUBDIRS))

define macroSubdirTargets
$(1):
	${MAKE} -C $(1)

$(1)_clean:
	${MAKE} -C $(1) clean
endef

$(foreach d, $(SUBDIRS), \
    $(eval $(call macroSubdirTargets,$(d))))

# creating the pdf

# displaying the document
show: $(ZETTEL_NAME).pdf
	$(VIEWER) $< & 

# the big chunk of work...
$(ZETTEL_NAME).pdf: subdirs $(TEX_ALL) main.bib
	$(PDFLATEX) $(OPTION) main.tex
	$(BIBTEX) main
	$(PDFLATEX) $(OPTION) main.tex
	$(PDFLATEX) $(OPTION) main.tex

clean: subdirs_clean
	rm -f texput.log; \
	rm -f *.aux; \
	for i in $(TEX_ENDINGS); do \
		rm -f $(ZETTEL_NAME).$$i; \
	done;

.PHONY: clean $(SUBDIRS) $(addsuffix _clean,$(SUBDIRS))
