#!/bin/bash

if [ $# -lt 1 ]; then
    echo -n "enter url: "
    read url
else
    url="$1"
fi
if [ $# -lt 2 ]; then
    echo -n "enter target pngName: "
    read pngName
else
    pngName="$2"
fi

# plumbing area

pngFileName=pix/"$pngName".png
origImageName=`basename $url`
# crude... this will need to be tuned...
imageTitle=$(echo "$origImageName" | sed 's/_/\\_/g' )

echo -n "downloading into '$pngFileName'..."
curl -s "$url" | convert - -transparent white "$pngFileName"

echo " done! use this:"
echo ""

cat << EOF
\\begin{tikzpicture}[remember picture,overlay]
    \\standardPicCite[3cm]{$pngName}
\\end{tikzpicture}
EOF

bibContent=$(cat << EOF
@misc{image:$pngName,
    title = "${imageTitle}",
    month = "`date +"%B"`",
    year = "`date +"%Y"`",
    howpublished = "{\\tiny\\url{$url}}"
},
EOF
)

echo ""
echo "appended the reference to 'main.bib':"
echo ""
echo "$bibContent"
# TODO: could even use 'sed' to find and modify an existing entry?
echo "$bibContent" >> main.bib
